<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'IUserService.php';
include_once 'User.php';
include_once 'File.php';



class UserService implements IUserService
{
    const db_host="localhost";
    const db_username="root";
    const db_password="";
    const db_name="mycloud";
    

    
    public function addUser($user) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        
       
        $res=$con->query("INSERT INTO user ( FirstName, LastName, Email, Password, FreeSpace, Confirm)"
                . " VALUES "
                . "( '$user->firstName', '$user->lastName', '$user->email', '$user->password', "
                . "'$user->freeSpace', $user->confirmNumber)");
        if ($res) {
            
        print("Dobro je proslo");
            
        }
        else
        {
            print ("Query failed");
        }
    }
    }



    public function getUser($username) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("select * from user where Email='$username'");
        if ($res) {
            $user = null;
            // fetch_assoc() pribavlja jedan po jedan red iz rezulata 
			// u redosledu u kom ga je vratio db server
            if ($row = $res->fetch_assoc()) {
				
				$user=new User($row['Id'],$row['FirstName'],$row['LastName'], $row['Email'],$row['Password'],
                                       $row['FreeSpace'],$row['Confirm']);
                                       

            }
            // zatvaranje objekta koji cuva rezultat
            
            return $user;
        }
        else
        {
            print ("Query failed");
        }
    }
    
    }

    public function userExist($email, $password) {
       $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
       
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("select * from user where Email='$email' and Password='$password'");
        if ($res) {
            $user = null;
            // fetch_assoc() pribavlja jedan po jedan red iz rezulata 
			// u redosledu u kom ga je vratio db server
            if ($row = $res->fetch_assoc()) {
				
				$user=new User($row['Id'],$row['FirstName'],$row['LastName'], $row['Email'],$row['Password'],
                                       $row['FreeSpace'],$row['Confirm']);
                                       

            }
            // zatvaranje objekta koji cuva rezultat
            
            return $user;
        }
        else
        {
            print ("Query failed");
        }
    }  
    }

    public function getUserById($id) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("select * from user where Id='$id'");
        if ($res) {
            $user = null;
            // fetch_assoc() pribavlja jedan po jedan red iz rezulata 
			// u redosledu u kom ga je vratio db server
            if ($row = $res->fetch_assoc()) {
				
				$user=new User($row['Id'],$row['FirstName'],$row['LastName'], $row['Email'],$row['Password'],
                                       $row['FreeSpace'],$row['Confirm']);
                                       

            }
            // zatvaranje objekta koji cuva rezultat
            
            return $user;
        }
        else
        {
            print ("Query failed");
        }
    }
    }

    public function changeUserData($id) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("UPDATE user SET Confirm=0 where Id='$id'");
        if ($res) {
            print ("Query ok");
        }
        else
        {
            print ("Query failed");
        }
    }
        
    }

    public function addFile($file) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        
       
        /*$res=$con->query("INSERT INTO files ( nameOfFile, content, crc, date, user)"
                . " VALUES "
                . "('$file->nameOfFile', '$file->content','$file->crc','$file->date','$file->user')");*/
        $res=$con->query("INSERT INTO files ( nameOfFile, crc, date, user)"
                . " VALUES "
                . "('$file->nameOfFile','$file->crc','$file->date','$file->user')");
        if ($res) {
            
        print("Dobro je proslo");
            
        }
        else
        {
            print ($file->content);
        }
    }
    }

    public function getFileIfExist($idUser, $fileName) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("select * from files where user=$idUser and nameOfFile='$fileName'");
        if ($res) {
            $file = null;
            // fetch_assoc() pribavlja jedan po jedan red iz rezulata 
			// u redosledu u kom ga je vratio db server
            if ($row = $res->fetch_assoc()) {
				
				$file=new File($row['id'],$row['nameOfFile'],$row['content'], $row['crc'],$row['date'],
                                       $row['user']);
                                $path=$row['externalLink'];
                                $myfile=fopen($path, "r");
                                $file->content=fread($myfile,filesize($path));
                                fclose($myfile);
          

            }
            // zatvaranje objekta koji cuva rezultat
            
            return $file;
        }
        else
        {
            print ("Query failed");
        }
    }
    }

    public function getFile($idUser, $idFile) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("select * from files where user='$idUser' and id='$idFile'");
        if ($res) {
            $file = null;
            // fetch_assoc() pribavlja jedan po jedan red iz rezulata 
			// u redosledu u kom ga je vratio db server
            if ($row = $res->fetch_assoc()) {
				
				$file=new File($row['id'],$row['nameOfFile'],$row['content'], $row['crc'],$row['date'],
                                       $row['user']);
                                $path=$row['externalLink'];
                                $myfile=fopen($path, "r");
                                $file->content=fread($myfile,filesize($path));
                                fclose($myfile);
            }
            // zatvaranje objekta koji cuva rezultat
            
            return $file;
        }
        else
        {
            print ("Query failed");
        }
    }
    }

    public function getAllFilesByUser($idUser) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("select * from files where user=$idUser");
        if ($res) {
            $files = array();
            // fetch_assoc() pribavlja jedan po jedan red iz rezulata 
			// u redosledu u kom ga je vratio db server
            while ($row = $res->fetch_assoc()) {
		
                $file = new File($row['id'],$row['nameOfFile'],$row['content'], $row['crc'],$row['date'],
                                       $row['user']);
                $path=$row['externalLink'];
                $myfile=fopen($path, "r");
                $file->content=fread($myfile,filesize($path));
                fclose($myfile);
                array_push($files, $file);
                                       

            }
            // zatvaranje objekta koji cuva rezultat
            
            return $files;
        }
        else
        {
            print ("Query failed");
        }
    }
    }

    public function getFreeSpace($idUser) {
    
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("select * from user where Id='$idUser'");
        if ($res) {
            $freeSpace = null;
            // fetch_assoc() pribavlja jedan po jedan red iz rezulata 
			// u redosledu u kom ga je vratio db server
            if ($row = $res->fetch_assoc()) {
				
				$freeSpace=$row['FreeSpace'];
                                       

            }
            // zatvaranje objekta koji cuva rezultat
            
            return $freeSpace;
        }
        else
        {
            print ("Query failed");
        }
    }
        
    }

    public function updateFreeSpace($idUser,$newFreeSpace) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        $res = $con->query("UPDATE user SET FreeSpace='$newFreeSpace' where Id='$idUser'");
        if ($res) {
            print ("Query ok");
        }
        else
        {
            print ("Query failed");
        }
    }
        
    }

    public function updateFile($idUser, $data, $dateAndTime) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        
       
        $res = $con->query("UPDATE files SET content='$data', date='$dateAndTime'  where id'$idUser'");
        if ($res) {
            
        print("Dobro je proslo");
            
        }
        else
        {
            print ("Query failed");
        }
    }
    }

    public function deleteFile( $idUser,$idFile) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        $fileSize=0;
        $freeSpace=0;
        // $res je rezultat izvrsenja upita
        $res1 = $con->query("select * from files where id='$idFile'");
        if ($res1) {
        
              if ($row = $res1->fetch_assoc()) {
                $path=$row['externalLink'];
                $myfile=fopen($path, "r");
                $file->content=fread($myfile,filesize($path));
                fclose($myfile);
		$fileSize=strlen($file->content);
                print("Dobro je proslo");
            
        }
        else
        {
            print ("Query failed");
        }
        $res2 = $con->query("select * from user where id='$idUser'");
        if ($res2) {
              if ($row = $res2->fetch_assoc()) {
				
				$freeSpace=$row['FreeSpace'];
              }
       
        $freeSpace+=$fileSize;
       
        $this->updateFreeSpace($idUser, $freeSpace);
        print("Dobro je proslo");
            
        }
        else
        {
            print ("Query failed");
        }
        $path=null;
        $res3 = $con->query("select * from files where id='$idFile'");
        if ($res3) {
              if ($row = $res3->fetch_assoc()) {
				
				$path=$row['externalLink'];
              }
       
        unlink($path);
        print("Dobro je proslo");
            
        }
        else
        {
            print ("Query failed");
        }
        
       
        $res4 = $con->query("DELETE from files where id='$idFile'");
        if ($res4) {
            
        print("Dobro je proslo");
            
        }
        else
        {
            print ("Query failed");
        }
    } 
    }
    }

    public function addExternalLink($idUser,$fileName,$path) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        
       
        $res=$con->query("UPDATE files SET externalLink='$path' WHERE user='$idUser' AND nameOfFile='$fileName'");
        if ($res) {
            
        print("Dobro je proslo");
            
        }
        else
        {
            print ($file->content);
        }
    }
    }

    public function getPathOfFile($idUser, $idFile) {
    $con = new mysqli(self::db_host, self::db_username, self::db_password, self::db_name);
    if ($con->connect_errno) {
        // u slucaju greske odstampati odgovarajucu poruku
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    }
    else {
        // $res je rezultat izvrsenja upita
        
       
        $res=$con->query("SELECT * FROM files WHERE user='$idUser' AND id=$idFile");
        if ($res) {
            
        $externalLink = null;
          if ($row = $res->fetch_assoc()) 
          {
                $externalLink=$row['externalLink'];
          }
        return  $externalLink; 
        }
        else
        {
            print ($file->content);
        }
    }
    }

}

