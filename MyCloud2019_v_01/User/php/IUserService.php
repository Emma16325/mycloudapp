<?php


interface IUserService {
    //put your code here
    function addUser($user);
    function getUser($username);
    function userExist($email,$password);
    function getUserById($id);
    function changeUserData($id);
    function getFileIfExist($idUser,$fileName);
    function addFile($file);
    function getFile($idUser,$idFile);
    function getAllFilesByUser($idUser);
    function getFreeSpace($idUser);
    function updateFreeSpace($idUser,$newFreeSpace);
    function updateFile($idUser,$data,$dateAndTime);
    function deleteFile($idUser,$idFile);
    function addExternalLink($idUser,$fileName,$path);
    function getPathOfFile($idUser, $idFile);
    
     
}
