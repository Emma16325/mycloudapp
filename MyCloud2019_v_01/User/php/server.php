<?php
include_once 'lib.php';

$chunkSize=120*1048576;//1MB ----> 1048576B
$base=new UserService();

$data="";
//------------------------------------- GET NAME OF FILE -------------------------------------------------------------------
if(isset($_POST["nameRequest"]))
{
    $file=$base->getFile($_POST["idUser"], $_POST["idFile"]);
    echo json_encode($file->nameOfFile);
}
//------------------------------------- GET CRC OF FILE ---------------------------------------------------------------------
if(isset($_POST["crcRequest"]))
{
    $file=$base->getFile($_POST["idUser"], $_POST["idFile"]);
    echo json_encode($file->crc);
}
//-------------------------------------- GET FILE CHUNK BY CHUNK ------------------------------------------------------------
if(isset($_POST["getFileChunkByChunk"]))
{
    $file=$base->getFile($_POST["idUser"], $_POST["idFile"]);
   
    if($_POST["start"]<$_POST["end"])
    { 
        $data.=substr($file->content, $_POST["start"],$chunkSize);
        echo json_encode($data);    
    }
    else
         echo json_encode($data);
}
//------------------------------------------ GET DOWNLOAD PARAMETARS ---------------------------------------------------------
if(isset($_POST["requestDownload"]))
{
     $sizes = array();
     array_push($sizes,$chunkSize);
     $file=$base->getFile($_POST["idUser"], $_POST["idFile"]);
     array_push($sizes,strlen($file->content));
    
     echo json_encode($sizes);
}
//------------------------------------------------ GET FILE ------------------------------------------------------------------
if(isset($_POST["getFile"]))
{
    $file=$base->getFile($_POST["idUser"], $_POST["idFile"]);
    echo json_encode($file);
}
if(isset($_POST["request"]))
{
   //klijent zahteva od nas ChunkSize
    echo json_encode($chunkSize);

} 
//------------------------------------------------- GET FILE IF EXIST ----------------------------------------------------------
else if(isset($_POST["fileExist"]))
{
    $file=$base->getFileIfExist($_POST["idUser"], $_POST["fileName"]);
    echo json_encode($file);
}
//-------------------------------------------------- DELETE FILE --------------------------------------------------------------- 
else if(isset($_POST["DeleteFile"]))
{

    $base->deleteFile($_POST["idUser"],$_POST["idFile"]);
    
}
//-------------------------------------------- UPLOAD FILE CHUNK BY CHUNK -------------------------------------------------------
else if( (isset($_POST["idUser"])) && (isset( $_POST["fileName"])))
{
   
    
    if($_POST["end"]<$_POST["size"])
    {
         $date=date("Y/m/d");
         $time=date("h:i:sa");
         $dateAndTime=$date." ".$time;
         //$data.=$_POST["data"];
        $path="../MyCloudStoreBase/".strval($_POST["idUser"])."/".$_POST["fileName"];
        $myfile = fopen($path, "a");
        $contentOfFile = $_POST["data"];
        fwrite($myfile, $contentOfFile);
        fclose($myfile);
        //echo json_encode("succ");
        
    }
    else
    {
        $data.=$_POST["data"];
        $date=date("Y/m/d");
        $time=date("h:i:sa");
        $dateAndTime=$date." ".$time;
        $freeSpace=($base->getFreeSpace($_POST["idUser"]));//freeSpace je u bajtovima
        $freeSpace-=strlen($_POST["data"]);
        $base->updateFreeSpace($_POST["idUser"], $freeSpace);
        //create new file and store in user directory

        $path="../MyCloudStoreBase/".strval($_POST["idUser"])."/".$_POST["fileName"];
        $myfile = fopen($path, "a");
        $contentOfFile = $_POST["data"];
        fwrite($myfile, $data);
        fclose($myfile);
        
        $file=new File(0,$_POST["fileName"],$data,$_POST["crc"],$dateAndTime,
                $_POST["idUser"]);
        $base->addFile($file);
        $base->addExternalLink($_POST["idUser"],$_POST["fileName"], $path);
        //echo json_encode("succ");
    }
    
}
// ------------------------------------------------- DOWNLOAD FILE --------------------------------------------------------------
else if(isset($_POST["download"]))
{
    $file=$base->getFile($_POST["idUser"], $_POST["idFile"]);
    $path=$base->getPathOfFile($_POST["idUser"], $_POST["idFile"]);
    $myfile=fopen($path, "r");
    $file->content=fread($myfile,filesize($path));

    fclose($myfile);
    echo json_encode($file);
}
// ------------------------------------------------- GET ALL FILES OF USER -------------------------------------------------------
else if(isset ($_POST["files"]))
{
    $files=null;
    $files=$base->getAllFilesByUser($_POST["idUser"]);
    echo json_encode($files);
}
else if(isset($_POST["FreeSpaceRequest"])&& isset($_POST["idUser"]))
{
    $freeSpace=null;
    $freeSpace=$base->getFreeSpace($_POST["idUser"]); //freeSpace u bajtovima
    echo json_encode($freeSpace);
}